package com.cibl.bottommenu.bottom_menu_activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.cibl.bottommenu.R;
import com.cibl.bottommenu.databinding.ActivityBottomMenuBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomMenuActivity extends AppCompatActivity {
    ActivityBottomMenuBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil. setContentView(this,R.layout.activity_bottom_menu);
        initComponent();
        initListener();
    }

    private void initListener() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.account:
                        Toast.makeText(BottomMenuActivity.this, "Account Action", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.home:
                        Toast.makeText(BottomMenuActivity.this, "Home Action", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.exit:
                        Toast.makeText(BottomMenuActivity.this, "Exit Action", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.help:
                        Toast.makeText(BottomMenuActivity.this, "Help Action", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    private void initComponent() {
        binding.bottomNavigationView.setBackground(null);

    }


}