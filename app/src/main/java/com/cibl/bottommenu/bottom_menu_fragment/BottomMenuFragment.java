package com.cibl.bottommenu.bottom_menu_fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cibl.bottommenu.R;
import com.cibl.bottommenu.bottom_menu_activity.BottomMenuActivity;
import com.cibl.bottommenu.databinding.FragmentBottomMenuBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;

public class BottomMenuFragment extends Fragment {
   FragmentBottomMenuBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_bottom_menu, container, false);
        initComponent();
        initListener();
        return binding.getRoot();
    }

    private void initListener() {
        binding.bottomNavigationViewFragment.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.account:
                        Toast.makeText(getActivity(), "Account Action Fragment", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.home:
                        Toast.makeText(getActivity(), "Home Action Fragment", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.exit:
                        Toast.makeText(getActivity(), "Exit Action Fragment", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.help:
                        Toast.makeText(getActivity(), "Help Action Fragment", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    private void initComponent() {
        binding.bottomNavigationViewFragment.setBackground(null);
        // We can handel the Bottom Menu In Using this process
        binding.bottomNavigationViewFragment.setSelectedItemId(R.id.help);
    }
}