package com.cibl.bottommenu.bottom_menu_fragment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cibl.bottommenu.R;
import com.cibl.bottommenu.bottom_menu_fragment.BottomMenuFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class BottomMenuFragmentsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottom_menu_fragments_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new BottomMenuFragment())
                    .commitNow();
        }
    }
}